<?php

require 'vendor/autoload.php';

use Google\Cloud\BigQuery\BigQueryClient;

$bigQuery = new BigQueryClient([
    'projectId' => 'dms-datatransfer-bigquery',
    'keyFilePath' => 'dms-datatransfer-bigquery-310551ddc02b.json',
        ]);

$queryJobConfig = $bigQuery->query('SELECT * '
        . 'FROM `dms-datatransfer-bigquery.DMSDT.uniliver7729`'
        . ' WHERE _DATA_DATE = DATE_ADD(CURRENT_DATE(), INTERVAL - 1 DAY) '
        . 'AND AdvertiserId IN( 4531395800) LIMIT 5 OFFSET 0');
$queryResults = $bigQuery->runQuery($queryJobConfig);
$data = array();
$d = array();
$head = array();
$count = 0;
foreach ($queryResults as $queryResult) {
    $head = array();
    $d = array();
    $count ++;
    foreach ($queryResult as $key => $value) {
        if ($count == 1) {
            $head[] = $key;
        }
        $d[] = $value;
    }
    if ($head) {
        $data[] = $head;
    }
    $data[] = $d;
}
$f_name = date('Ymd',strtotime("-1 days"));
$filename = $f_name.'.csv';
$file = fopen($filename, 'w');
// save each row of the data
foreach ($data as $row) {
    fputcsv($file, $row);
}
fclose($file);

$zip_file = $f_name.'.zip';
$zip = new ZipArchive();
$zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);
$zip->addFile($filename, $filename);
$zip->close();